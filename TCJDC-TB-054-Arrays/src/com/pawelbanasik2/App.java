package com.pawelbanasik2;

import java.util.Scanner;

public class App {

	// ten static tu jest KLUCZOWY bo inaczej skaner w metodzie nie bedzie dziala�!!!!!!!!!!!!!!!!!!!!!!!!!!!
	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		int[] myIntegers = createArray(5);
		for (int i = 0; i < myIntegers.length; i++) {
			System.out.println("Element " + i + ", typed value was " + myIntegers[i]);

		}
		
		System.out.println("The average is " + getAverage(myIntegers));
	}

	public static int[] createArray(int number) {
		System.out.println("Enter " + number + " integer values.\r ");
		int[] array = new int[number];
		for (int i = 0; i < array.length; i++) {

			array[i] = scanner.nextInt();

		}
		return array;
	}

	public static double getAverage(int[] array) {

		int sum = 0;
		for (int i = 0; i < array.length; i++) {
			sum += array[i];

		}
		return (double) sum / (double) array.length;
	}

}
